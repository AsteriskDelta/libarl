#ifndef ARL_STACK_H
#define ARL_STACK_H
#include "Positional.h"

namespace arl {
    class Cell;
    
    class Stack :   public virtual Proxy,
                    public Positional {
    public:
        virtual ~Stack();
        
        Cell* get(MapComponent y, bool create = false);
        inline auto operator[](MapComponent y) {
            return this->get(y);
        }
        
        struct {
            Position min, max;
        } bounds;
    protected:
        struct CellData {
            MapComponent y;
            Cell *cell;
        };
        std::vector<CellData> cells;
    }
}

#endif
