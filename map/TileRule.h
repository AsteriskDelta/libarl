#ifndef ARL_TILE_RULE_H
#define ARL_TILE_RULE_H

namespace arl {
    class TileRule {
    public:
        virtual ~TileRule() {
            
        }
        
        std::vector<std::vector<Tile*>> pattern;
        MapCoord center;
        
        struct {
            ColorRGBA bg, fg;
            ColorRGBA tintMask;
        } color;
        
        std::vector<std::vector<wchar_t>> replacement;
        
        bool matches(Map *map, MapCoord pos) const;
    protected:
        
    }
}

#endif

