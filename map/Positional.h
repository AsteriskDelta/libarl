#ifndef ARL_POSITIONAL_H
#define ARL_POSITIONAL_H
#include "Position.h"

namespace arl {
    class Positional {
    public:
        Position position;
        Orientation orientation;
    protected:
        
    };
}

#endif
