#ifndef ARL_TILE_H
#define ARL_TILE_H
#include "Identified.h"
#include "TileRule.h"
#include "Instance.h"

namespace arl {
    class Tile;
    
    /*struct TileInstance {
        Tile *tile;
        inline operator bool() const {
            return tile != nullptr;
        }
    };*/
    typedef Instance<Tile> TileInstance;
    
    class Tile :    public Identified<Tile*>, 
                    public Named {
    public:
        _self(Tile*);
        
        Tile *parent;
        
        char* evaluate(Map *map, MapCoord pos) const;
        
        bool matches(Tile *o) const;
    protected:
        std::vector<TileRule> rules;
    }
}

#endif
