#ifndef ARL_POSITION_H
#define ARL_POSITION_H

namespace arl {
    typedef MapCoord Position;
    typedef Num3v Orientation;
}

#endif
