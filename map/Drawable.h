#ifndef ARL_DRAWABLE_H
#define ARL_DRAWABLE_H
#include <ARKE/Proxy.h>

namespace arl {
    class Drawable : public virtual Proxy {
    public:
        virtual void draw();
    protected:
        
    };
}

#endif
