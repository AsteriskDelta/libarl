#ifndef ARL_CANVAS_H
#define ARL_CANVAS_H
#include <ARKUI/Style.h>

namespace arl {
    class Canvas : public ui::Object {
    public:
        struct Char {
            wchar_t data;
            ui::Style style;
        };
        
        //std::vector<Char>& operator[](MapCoord rel);
        
        //void compose(Position focus);
        virtual void clear(const Char bg);
        
        virtual void draw() override;
        
        virtual void resize(unsigned int w, unsigned int h, bool preserve = false) override;
        
        inline unsigned int width() const {
            return width_;
        }
        inline unsigned int height() const {
            return height_;
        }
        
        inline Char& operator[](const MapCoord& c) {
            return data[this->dc(c.x, c.y)];
        }
        inline const Char& operator[](const MapCoord& c) const {
            return data[this->dc(c.x, c.y)];
        }
    protected:
        std::vector<TileInstance> data;
        unsigned int width_, height_;
        std::vector<Char> data;
        
        inline unsigned int dc(unsigned int x, unsigned int y) const {
            return x + y * this->width();
        }
    };
}

#endif
