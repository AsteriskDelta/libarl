#ifndef ARL_MAP_H
#define ARL_MAP_H
#include "Position.h"

namespace arl {
    class Stack;
    
    class MapNode : public virtual Proxy
        virtual ~MapNode();
        
        Position min, max;
        
        inline bool leaf() const {
            return children.empty();
        }
        
    protected:
        std::vector<MapNode*> children;
        std::vector<Stack*> stacks;
    };
    
    class Map : public virtual Proxy {
    public:
        MapNode *root;
    };
}

#endif
