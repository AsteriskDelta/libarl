#ifndef ARL_CELL_H
#define ARL_CELL_H
#include <ARKE/Proxy.h>
#include "Tile.h"

namespace arl {
    class Cell : public virtual Proxy {
    public:
        virtual ~Cell();
        
        void resize(unsigned int w, unsigned int h, bool preserve = false);
        
        inline unsigned int width() const {
            return width_;
        }
        inline unsigned int height() const {
            return height_;
        }
        
        
        inline TileInstance& operator[](const MapCoord& c) {
            return data[this->dc(c.x, c.y)];
        }
        inline const TileInstance& operator[](const MapCoord& c) const {
            return data[this->dc(c.x, c.y)];
        }
    protected:
        std::vector<TileInstance> data;
        unsigned int width_, height_;
        
        inline unsigned int dc(unsigned int x, unsigned int y) const {
            return x + y * this->width();
        }
        
    };
}

#endif
