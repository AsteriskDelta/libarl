#ifndef ARKE_INSTANCE_H
#define ARKE_INSTANCE_H

namespace arke {
    template<typename KEY>
    struct InstanceBase {
        KEY *key;
        inline bool valid() const {
            return key != nullptr;
        }
        inline operator bool() const {
            return this->valid();
        }
        
        inline bool is(KEY *const k) {
            return k == key;
        }
    };
    
    template<typename KEY>
    struct Instance : InstanceBase<KEY> {
    public:
        
    protected:
        
    };
}

#define INSTANCE(T) template<> struct Instance<T> : InstanceBase<T> 

#endif
