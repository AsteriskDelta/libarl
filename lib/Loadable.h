#ifndef ARKE_LOADABLE_H
#define ARKE_LOADABLE_H

/*
#define LOAD_ALL_FN(TYPE) \
    template<>\
    static int Loadable::LoadAll<TYPE>(const std::string& dirPath, bool abs)
*/

namespace arke {
    class JSON;
    
    class Loadable {
    public:
        virtual ~Loadable();
        
        virtual bool load(const std::string& dataPath, bool abs = false);
        virtual bool save(const std::string& dataPath, bool abs = false);
        
        virtual void onLoad();
        
        virtual bool dirty() const;
        virtual void setDirty(bool st = true);
        
        virtual Loadable* clone() const = 0;
        
        static int LoadAll(Loadable *prototype, const std::string& dirPath, bool abs = false);
        
        template<typename T>
        static inline int LoadAll(const std::string& dirPath, bool abs = false) {
            T* proto = new T();
            int ret = LoadAll(proto, dirPath, abs);
            delete proto;
            
            return ret;
        }
    protected:
        virtual void writeData(JSON *json);
        virtual void readData(JSON *json);
    private:
        bool dirty_;
    };
}

#endif
