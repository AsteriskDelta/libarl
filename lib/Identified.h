#ifndef ARKE_IDENTIFIED_H
#define ARKE_IDENTIFIED_H

#define _self(T) \
    inline virtual T self() {\
        return this;\
    }\
    using Identified<T>::ByID;
/*\
    inline virtual const T self() const override {\
        return this;\
    }*/

#define IDENTIFIED(T) namespace arke {\
    template<>\
    std::unordered_map<std::string, T*> Identified<T*>::IDMap;\
};

namespace arke {
    class Named {
    public:
        inline const std::string& name() const {
            return this->name_;
        }
        inline const std::string& name(const std::string &nn) {
            this->name_ = nn;
            return this->name();
        }
    protected:
        std::string name_;
    };
    
    template<typename T>
    class Identified {
    public:
        inline virtual ~Identified() {
            this->unregisterIdentity();
        }
        
        inline const std::string& id() const {
            return this->id_;
        }
        inline const std::string& id(const std::string n) {
            this->id_ = n;
            this->registerIdentity();
            return this->id();
        }
        
        inline static T ByID(const std::string& idStr) {
            auto it = IDMap.find(idStr);
            if(it == IDMap.end()) return nullptr;
            else return it->second;
        }
        
        inline bool identityRegistered() const {
            return !this->id().empty() && IDMap.find(this->id()) != IDMap.end();
        }
        
        /*inline virtual T self() {
            return nullptr;
        }
        inline virtual const T self() const {
            return nullptr;
        }*/
    protected:
        std::string id_;
        static std::unordered_map<std::string, T> IDMap;
        
        inline void registerIdentity() {
            if(this->identityRegistered()) this->unregisterIdentity();
            IDMap[this->id()] = static_cast<T>(this);
        }
        inline void unregisterIdentity() {
            auto it = IDMap.find(this->id());
            if(it != IDMap.end()) IDMap.erase(it);
        }
        
    };
    
    template<typename T>
    std::unordered_map<std::string, T> Identified<T>::IDMap;
}

#endif
