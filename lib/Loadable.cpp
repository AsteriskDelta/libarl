#include "Loadable.h"
#include <ARKE/AppData.h>
#include <ARKE/Console.h>
#include <ARKE/JSON.h>

namespace arke {
    Loadable::~Loadable() {
        
    }
    
    bool Loadable::load(const std::string& dataPath, bool abs) {
        unsigned int len;
        char* data;
        if(abs) {
            data = AppData::system.readFileText(dataPath, len);
        } else {
            data = AppData::root.readFileText(dataPath, len);
        }
        
        if(data == nullptr) {
            Console::Err("Loadable at \"", dataPath, "\" could not be opened for reading");
            return false;
        }
        
        std::string str(data);
        std::string errStr;
        
        JSON *json = new JSON();
        bool success = json->deserialize(str, &errStr);
        
        if(!success) {
            Console::Err("Loadable at \"", dataPath, "\" contained invalid JSON data");
            return false;
        }
        
        bool ret = true;
        try {
            this->readData(json);
        } catch(...) {
            ret = false;
        }
        delete json;
        delete data;
        
        if(ret) {
            this->setDirty(false);
            this->onLoad();
        }
        
        return ret;
    }
    bool Loadable::save(const std::string& dataPath, bool abs) {
        JSON *json = new JSON();
        
        try {
            this->writeData(json);
        } catch(...) {
            delete json;
            return false;
        }
        
        std::string ser = json->serialize();
        delete json;
        
        bool ret = false;
        if(abs) {
            ret = AppData::system.writeFile(dataPath, ser.c_str(), ser.size());
        } else {
            ret = AppData::root.writeFile(dataPath, ser.c_str(), ser.size());
        }
        
        if(!ret) {
            Console::Err("Failed to write Loadable to \"", dataPath, "\"");
            return false;
        } else {
            this->setDirty(false);
            return true;
        }
    }
    
    void Loadable::onLoad() {
        
    }
    
    bool Loadable::dirty() const {
        return dirty_;
    }
    void Loadable::setDirty(bool st) {
        dirty_ = st;
    }
    
    int Loadable::LoadAll(Loadable *prototype, const std::string& dirPath, bool abs) {
        int loadCount = 0;
        auto cb = [prototype, abs, &loadCount](std::string name, std::string fullPath) -> bool {
            _unused(fullPath);
            Loadable *loaded = prototype->clone();
            
            bool success = loaded->load(name, abs);
            if(!success) {
                delete loaded;
                return false;
            }
            
            loadCount++;
            return true;
        };
        
        bool listed = false;
        if(abs) {
            listed = AppData::system.listCallback(cb, true, dirPath);
        } else {
            listed = AppData::root.listCallback(cb, true, dirPath);
        }
        
        if(!listed) {
            Console::Err("Failed to list contents of Loadable directory \"", dirPath, "\"");
            return -1;
        }
        return loadCount;
    }
    
    void Loadable::writeData(JSON *json) {
        _unused(json);
    }
    void Loadable::readData(JSON *json) {
        _unused(json);
    }
};
