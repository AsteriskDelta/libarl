#ifndef ARKE_HIERARCHY_H
#define ARKE_HIERARCHY_H
#include <unordered_set>

namespace arke {
    template<typename T>
    class Hierarchy {
    public:
        T parent;
        std::unordered_set<T> children;
        
        inline virtual void addChild(T child) {
            children.insert(child);
            child->setParent(static_cast<T>(this));
        }
        
        inline virtual void removeChild(T child) {
            auto it = children.find(child);
            if(it != children.end()) {
                children.erase(it);
                child->parent = nullptr;
            }
            
        }
        
    protected:
        inline virtual void setParent(T par) {
            if(parent != nullptr) parent->removeChild(static_cast<T>(this));
            parent = par;
            //parent->addChild(static_cast<T>(this));
        }
        
    }
}

#endif
