#pragma once
#include <NVX/NVX.h>
#include <Spinster/Spinster.h>
#include <ARKE/ARKE.h>

using namespace nvx;
using namespace arke;
typedef NI2<24,8,0> Num;
typedef NVX2<3, Num> Num3v;
typedef NI2<32,0,0> MapComponent;
typedef NVX2<3, MapComponent> MapCoord;

namespace arl {
    class Entity;
    class Attribute;
    class Stat;
    class Effect;
    
    class Map;
    class Tile;
}
