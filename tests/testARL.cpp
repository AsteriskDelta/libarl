#include "Stat.h"
#include "Entity.h"

#include <ARKE/Application.h>
using namespace arl;

void Init() {//Called immediatly on execution
    //isServer = isClient = true;
    Application::BeginInitialization();
    //System::SetrLimit(1024*1024*1024);
    Application::SetDataDir("../../data/");
    Application::SetName("ARL");
    Application::SetCodename("arl01");
    Application::SetCompanyName("Delta III Tech");
    Application::SetVersion("0.0r1X");
    Application::SetDebug(true);
    //Application::OnExit(&GameManager::Cleanup);
    //System::SetStackLimit(1024*1024*256);
    Application::EndInitialization();

    //ImageFont::Initialize();
    //Input::Initialize();
    //ProgramStat::StartRecorderThread("prstat/main");
}

int main(int argc, char** argv) {
    _unused(argc, argv);
    Init();
    int cnt = Loadable::LoadAll<Stat>("stats");
    std::cout << "loaded " << cnt << " stats\n";
    
    Entity *player = new Entity();
    player->get(Stat::ByID("health")).value = 10;
    /*for(auto it = Loadable::Begin(); it != Loadable::End(); ++it) {
        std::cout << it->
    }*/
    return 0;
}
