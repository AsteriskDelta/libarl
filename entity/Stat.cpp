#include "Stat.h"
#include <ARKE/JSON.h>

namespace arl {
    void Stat::onLoad() {
        std::cout << "\t" << this->id() << "\t" << this->abbrv << "\t" << this->name() << "\n";
    }
    
    void Stat::writeData(JSON *json) {
        _unused(json);
    }
    void Stat::readData(JSON *json) {
        
        this->abbrv = json->get<std::string>("abbrv");
        this->transient = json->get<bool>("transient");
        
        this->name(json->get<std::string>("name"));
        this->id(json->get<std::string>("id"));
    }
    
    Stat* Stat::clone() const {
        return new Stat();
    }
}
