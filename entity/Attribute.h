#ifndef ARL_ATTRIBUTE_H
#define ARL_ATTRIBUTE_H
#include "Instance.h"
#include "Identified.h"

namespace arl {
    class Attribute;
    class Attribute :   public Identified<Attribute*>, 
                        public Named {
    public:
        _self(Attribute*);
        
        typedef int16_t Type;
        static constexpr const Type
            Invalid     = -1,
            Numeric     = 0,
            String      = 1,
            Printf      = 2;
        
        virtual ~Attribute();
    protected:
        
    };
}

namespace arke {
    INSTANCE(arl::Attribute) {
        Num level;
        std::string str;
        arl::Attribute::Type type;
    };
}

#endif

