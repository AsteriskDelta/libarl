#ifndef ARL_EFFECT_H
#define ARL_EFFECT_H
#include "Instance.h"
#include "Identified.h"

namespace arl {
    class Effect;
    class Effect : public Identified<Effect*>, public Named {
    public:
        _self(Effect*);
        
        virtual ~Effect();
    protected:
        
    };
}

namespace arke {
    INSTANCE(arl::Effect) {
        
    };
}

#endif
