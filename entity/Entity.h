#ifndef ARL_ENTITY_H
#define ARL_ENTITY_H
#include <ARKE/Proxy.h>
#include "Holder.h"
#include "Stat.h"
#include "Attribute.h"
#include "Effect.h"
#include "Item.h"
#include "Positional.h"

namespace arl {
    class Entity :  public virtual Proxy,
                    public Holder<Stat>,
                    public Holder<Attribute>,
                    public Holder<Effect>,
                    public Holder<Item>,
                    public Positional {
    public:
        Entity();
        virtual ~Entity();
        
        //Whhhyyyy do you betray me C++...
        using Holder<Stat>::get;
        using Holder<Effect>::get;
        using Holder<Attribute>::get;
        using Holder<Item>::get;
        
        virtual void update();
    protected:
        
    };
};

#endif
