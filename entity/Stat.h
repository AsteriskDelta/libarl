#ifndef ARL_STAT_H
#define ARL_STAT_H
#include <unordered_map>
#include "Instance.h"
#include "Identified.h"
#include "Loadable.h"

namespace arl {
    class Stat;
    class Stat :    public Identified<Stat*>,
                    public Named,
                    public Loadable {
    public:
        _self(Stat*);
        
        virtual void onLoad() override;
        
        virtual Stat* clone() const override;
        
        bool transient;
        std::string abbrv;
    protected:
        virtual void writeData(JSON *json) override;
        virtual void readData(JSON *json) override;
    };
}

namespace arke {
    INSTANCE(arl::Stat) {
        Num value = 0, max = -1;
    };
}

#endif
