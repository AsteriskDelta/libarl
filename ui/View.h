#ifndef ARL_VIEW_H
#define ARL_VIEW_H
#include <ARKUI/Object.h>

namespace arl {
    class View : public ui::Object {
    public:
        struct {
            uimvec size, position;
            bool sizeEnable, positionEnable;
        } ratios;
        
        virtual ~View();
        
        //virtual void draw() = 0;
    protected:
        
    };
}

#endif
