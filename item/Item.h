#ifndef ARL_ITEM_H
#define ARL_ITEM_H
#include "Identified.h"
#include "Holder.h"
#include "Attribute.h"
#include "Stat.h"

namespace arl {
    class Item;
    class Item :public Identified<Item*>,
                public Named,
                public Holder<Attribute>,
                public Holder<Stat> {
    public:
        _self(Item*);
    protected:
        
    };
}

namespace arke {
    INSTANCE(arl::Item) ,  public Holder<arl::Attribute>,
                           public Holder<arl::Stat> {
        int quantity;
    };
}

#endif
